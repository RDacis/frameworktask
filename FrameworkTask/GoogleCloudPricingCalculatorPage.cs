﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Internal;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System.Xml.Linq;

namespace FrameworkTask
{
    public class GoogleCloudPricingCalculatorPage
    {
        public GoogleCloudPricingCalculatorPage(IWebDriver browser)
        {
            PageFactory.InitElements(browser, this);
        }

        [FindsBy(How = How.Id, Using = "tab-item-1")]
        public IWebElement ComputeEngineTab { get; set; } = null!;
        [FindsBy(How = How.Id, Using = "input_100")]
        public IWebElement InstancesMeaningInput { get; set; } = null!;
        [FindsBy(How = How.Id, Using = "input_99")]
        public IWebElement NumberOfInstancesInput { get; set; } = null!;
        [FindsBy(How = How.Id, Using = "select_value_label_91")]
        public IWebElement OperatingSystemSelect { get; set; } = null!;
        [FindsBy(How = How.Id, Using = "myFrame")]
        public IWebElement PricingCalculator { get; set; } = null!;
        [FindsBy(How = How.XPath, Using = "/html/body/md-content/md-card/md-toolbar/div/div[1]/h2")]
        public IWebElement PricingCalculatorTitle { get; set; } = null!;
        [FindsBy(How = How.XPath, Using = "//*[@id=\"cloud-site\"]/devsite-iframe/iframe")]
        public IWebElement PricingCalculatorWrapper { get; set; } = null!;
        [FindsBy(How = How.Id, Using = "select_value_label_92")]
        public IWebElement ProvisioningModelSelect { get; set; } = null!;
        [FindsBy(How = How.XPath, Using = "select_container_125")]
        public IWebElement SeriesSelect { get; set; } = null!;
        [FindsBy(How = How.Id, Using = "select_option_221")]
        public IWebElement SeriesSelectOptionN1 { get; set; } = null!;

        public void FillOutForm(string quantity)
        {
            NumberOfInstancesInput.Clear();
            NumberOfInstancesInput.SendKeys(quantity);
            InstancesMeaningInput.Clear();
            ValidateClassExistence(InstancesMeaningInput, "ng-empty");
            ValidateText(OperatingSystemSelect, "Free: Debian");
            ValidateText(ProvisioningModelSelect, "Regular");
            SelectOption(SeriesSelect, "N1");
        }
        public void SelectOption(IWebElement webElement, string value)
        {
            SeriesSelect.Click();
            SeriesSelectOptionN1.Click();
            ValidateText(webElement, value);
        }
        public void ValidateClassExistence(IWebElement webElement, string expectedClass)
        {
            Assert.IsTrue(ContainsClass(webElement, expectedClass), $"{webElement} does not contain '{expectedClass}' class.");
        }
        public void ValidateText(IWebElement webElement, string expectedText)
        {
            Assert.IsTrue(webElement.Text.Contains(expectedText), $"{webElement} doesn't contain the specified text.");
        }
        private static bool ContainsClass(IWebElement element, string className)
        {
            return element.GetAttribute("class").Split(' ').Contains(className);
        }
    }
}
