﻿using SeleniumExtras.PageObjects;
using OpenQA.Selenium;

namespace FrameworkTask
{
    public class GoogleCloudLandingPage
    {
        private readonly IWebDriver driver;
        private readonly string url = @"https://cloud.google.com";

        public GoogleCloudLandingPage(IWebDriver browser)
        {
            driver = browser;
            PageFactory.InitElements(browser, this);
        }

        [FindsBy(How = How.CssSelector, Using = "input.mb2a7b[placeholder='Search']")]
        public IWebElement SearchBox { get; set; } = null!;

        public void Navigate()
        {
            driver.Navigate().GoToUrl(url);
        }
        public void Search(string textToType)
        {
            SearchBox.Click();
            SearchBox.Clear();
            SearchBox.SendKeys(textToType);
            SearchBox.SendKeys(Keys.Enter);
        }
    }
}
