﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace FrameworkTask
{
    public class GoogleCloudSearchResultPage
    {
        public GoogleCloudSearchResultPage(IWebDriver browser)
        {
            PageFactory.InitElements(browser, this);
        }

        [FindsBy(How = How.XPath, Using = "//*[@id=\"___gcse_0\"]/div/div/div/div[5]/div[2]/div/div/div[1]/div[1]/div[1]/div[1]")]
        public IWebElement SearchResultFirstItemThumbnail { get; set; } = null!;
        [FindsBy(How = How.XPath, Using = "//*[@id=\"___gcse_0\"]/div/div/div/div[5]/div[2]/div/div/div[1]/div[1]/div[1]/div[1]/div/a")]
        public IWebElement SearchResultFirstItemLink { get; set; } = null!;

        public void ClickFirstSearchResult()
        {
            ClickSearchResult(SearchResultFirstItemLink);
        }
        public void ValidateSearchResult(string expectedThumbnail)
        {
            Assert.IsTrue(SearchResultFirstItemThumbnail.Text.Equals(expectedThumbnail), "The first search result doesn't equal the specified text.");
        }
        private static void ClickSearchResult(IWebElement element)
        {
            element.Click();
        }
    }
}
