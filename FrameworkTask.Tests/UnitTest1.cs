using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using OpenQA.Selenium.Edge;

namespace FrameworkTask.Tests
{
    [TestClass]
    public class UnitTest1
    {
        public required IWebDriver Driver { get; set; }
        public required WebDriverWait Wait { get; set; }

        [TestInitialize]
        public void SetupTest()
        {
            Driver = new EdgeDriver();
            Wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(30));
        }

        [TestCleanup]
        public void TeardownTest()
        {
            Driver.Quit();
        }

        [TestMethod]
        public void SearchTextInSearchEngine_First()
        {
            GoogleCloudLandingPage googleCloudLandingPage = new(Driver);
            googleCloudLandingPage.Navigate();
            googleCloudLandingPage.Search("Google Cloud Platform Pricing Calculator");
            GoogleCloudSearchResultPage googleCloudSearchResultPage = new(Driver);
            googleCloudSearchResultPage.ValidateSearchResult("Google Cloud Pricing Calculator");
            googleCloudSearchResultPage.ClickFirstSearchResult();
            GoogleCloudPricingCalculatorPage googleCloudPricingCalculatorPage = new(Driver);
            Driver.SwitchTo().Frame(googleCloudPricingCalculatorPage.PricingCalculatorWrapper);
            Driver.SwitchTo().Frame(googleCloudPricingCalculatorPage.PricingCalculator);
            googleCloudPricingCalculatorPage.ValidateText(googleCloudPricingCalculatorPage.PricingCalculatorTitle, "Google Cloud Pricing Calculator");
            googleCloudPricingCalculatorPage.ValidateClassExistence(googleCloudPricingCalculatorPage.ComputeEngineTab, "md-active");
            googleCloudPricingCalculatorPage.FillOutForm("4");
        }
    }
}